# closeme
Simple and annoying JavaFX application.

Shows a window with an annoying blinking background. It also tries its best to not be closed by the user. 

## Build

Build the application by running the gradle wrapper script with the build task.

Run the shell script for *nix systems.
```
./gradlew build
```

Windows users should run the batch script.
```
gradlew.bat build
```


## Run

Run the jar located in `build/libs`.
```
java -jar closeme-1.0.0.jar
```

Stop the application via `Ctrl` + `C`.