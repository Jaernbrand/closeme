package com.jaernbrand.closeme;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        stage = new CatchMeStage();
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
