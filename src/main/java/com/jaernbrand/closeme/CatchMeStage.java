package com.jaernbrand.closeme;

import javafx.animation.*;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.util.Random;
import javafx.beans.property.ObjectProperty;

public class CatchMeStage extends Stage {

    private final double width;
    private final double height;

    private Random random = new Random();

    public CatchMeStage() {
        this.setTitle("Close me");

        Text text = createCenterText();

        BorderPane root = new BorderPane();
        root.setCenter(text);

        width = text.getLayoutBounds().getWidth() * 1.2;
        height = text.getLayoutBounds().getHeight() * 2;

        Scene scene = new Scene(root, width, height);
        this.setScene(scene);

        this.setResizable(false);
        this.positionRandomly();

        this.addEventHandler(MouseEvent.MOUSE_ENTERED_TARGET, (MouseEvent event) -> {
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getBounds();

            this.setX( random.nextInt((int)(bounds.getMaxX() - width)) );
            this.setY( random.nextInt((int)(bounds.getMaxY() - height)) );
        });


        this.setOnCloseRequest((event) -> {
            event.consume();
            multiply();
            close();
        });

        FillAnimation fillAnimation = new FillAnimation(scene.fillProperty());
        fillAnimation.start();
    }

    private Text createCenterText(){
        Text text = new Text("Close me if you can!! :D :P");
        text.setStroke(Color.BLACK);
        text.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 42D));
        text.setFill(Color.RED);
        return text;
    }

    private void positionRandomly(){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getBounds();

        setX( random.nextInt((int)(bounds.getMaxX() - width)) );
        setY( random.nextInt((int)(bounds.getMaxY() - height)) );
    }

    private void multiply() {
        new CatchMeStage().show();
        new CatchMeStage().show();
    }

    static class FillAnimation extends AnimationTimer {

        private final Color[] colors = {Color.YELLOW, Color.CYAN, Color.LIME, Color.MAGENTA};

        private long last;

        // In nanoseconds
        private final long interval = 100L * 1000_000L;

        private int currIdx = -1;

        private ObjectProperty<Paint> fillProperty;

        FillAnimation(ObjectProperty<Paint> fillProperty){
            this.fillProperty = fillProperty;
        }

        @Override
        public void handle(long now) {
            if (now - last >= interval){
                currIdx = ++currIdx % colors.length;
                fillProperty.setValue(colors[currIdx]);
                last = now;
            }
        }
    }

}
